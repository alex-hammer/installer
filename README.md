в composer.json itgro/extensions можно указывать install-path модуля айтигро в extra-settings, если не указать,
то по-умолчанию путь будет ./local/modules/
```$xslt
{
  "minimum-stability": "dev",
  "require": {
    "itgro/extensions": "dev-master"
  },
  "extra": {
    "itgro-modules-install-path": "../www/local/modules/"
  }
}
```

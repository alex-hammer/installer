<?php

namespace Itgro\Composer\Installer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;
use Composer\Factory as ComposerFactory;

class Installer extends LibraryInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        $installPath = 'local/modules/';

        if ($this->composer->getPackage()) {
            $extra = $this->composer->getPackage()->getExtra();

            if (isset($extra['itgro-modules-install-path'])) {
                $installPath = dirname(ComposerFactory::getComposerFile()) . '/' . $extra['itgro-modules-install-path'];
            }
        }

        $prettyName = $package->getPrettyName();

        if (strpos($prettyName, '/') !== false) {
            list($vendor, $name) = explode('/', $prettyName);
        } else {
            $name = $prettyName;
        }

        return $installPath . $name;
    }

    /**
     * {@inheritDoc}
     */
    protected function installCode(PackageInterface $package)
    {
        $sanitizedPath = $this->sanitizePath(str_replace('\\', '/', $this->getInstallPath($package)));
        $line = str_pad('***', strlen($sanitizedPath) + 36, '*');
        echo "\033[35;1;7m{$line}\n  Installing module itgro into \"{$sanitizedPath}\"…  \n{$line}\033[0m \n";

        parent::installCode($package);
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return 'itgro-module' === $packageType;
    }

    /**
     * @param $path
     * @return string
     */
    private function sanitizePath($path)
    {
        $parts = explode('/', $path);
        $index = array_search('..', $parts);
        if (false !== $index) {
            unset($parts[$index], $parts[$index-1]);

            return $this->sanitizePath(implode('/', $parts));
        }

        return implode('/', $parts);
    }
}